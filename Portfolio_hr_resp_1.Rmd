---
title: "Assignment 4 - Coordinating Heart Rate"
author: "Kathrine, Signe, Frederikke"
date: "November 2018
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Analysing Heart Rate and Respiration data

The goal of this assignment is to first familiarize you with heart rate, and respiration data and their preprocessing. The second part explores how to analyze interpersonal coordination of these signals.

These are the questions you need to be able to answer at the end of the assignment (aka that you need to submit as part of the portfolio)

1) How do you preprocess heart rate and respiration data? Describe the process. If any data needs to be excluded, list the excluded data and motivate the exclusion.

2) Do you observe interpersonal coordination in heart rate and respiration? Describe your control baseline, the method used to quantify coordination, and the statistical models used to infer whether coordination was higher than in the baseline. Report the results of the models.

3) Do you observe differences in coordination between conditions? Report the models and results.

4) Is respiration coordination a likely driver of heart rate coordination? Describe how you would test for it. Bonus points if you actually run the tests and report methods and results.

N.B. Because of the timing, we're starting this exercise before collecting the data.
Instead, you will develop your script this week on data from two years ago (Study1) and last year (Study2).
When you hand in the assignment for feedback, you can use the old data. But when you hand in the final version for the exam, you need to adapt your script to use the data we collect next week in the lab.
(For the old data): Note that synchronouns and turn-taking are the same task across both studies, but the third condition is different: two years ago it was self-paced joint reading; last year it was tv-series conversation.

NB: For this exercise, you will need to do something very similiar to what you've done before spread over several weeks. Ie parse data, look at the plots, decide on data cleaning, build a model, and finally evaluate and interpret the results of the models. Going back and copying the approach from previous exercises will likely be a great help.

## Step by step suggestions to solve the assignment


```{r}

#load libraries and data

library(tidyverse)
library(ggplot2)
library(caret)
library(dplyr)
library(stringr)
library(lme4)
#install.packages("robustHD")
library(robustHD)
#install.packages("psycho")
library(psycho)
library(boot)
#install.packages("pROC")
library(pROC)
library(cvms)
library(groupdata2)
library(data.table)
library(knitr)
#install.packages("gridExtra")
library(gridExtra)
#install.packages("magrittr")
library(magrittr)

#install.packages("crqa")
library(crqa)

# p_load(CRAN)
#install.packages("maps")
library(maps)


```


```{r}

####### DO NOT RUN AGAIN ##########################################################

##file list

files = list.files(pattern="Study*", path ="assignment4_data", full.names=TRUE)

#empty list
hr= list()

#function to read 

read_hr <- function(filename){
  d=read.delim(filename, sep=",")
  print(filename)
  data.frame(filename = filename, study = str_extract(filename, "Study\\d+"), group = str_extract(filename, "G\\d+"), trial =    str_extract(filename, "T\\d+"), condition = str_extract(filename, "[A-Za-z]+\\."), time = d$TimeMS, resp1 = d$Resp1, resp2 = d$Resp2, ecg1 = d$ECG1, ecg2 = d$ECG2, hr1 = d$HR1, hr2 = d$HR2)
             }


test= read_hr("assignment4_data/Study3_G1_T1_Synchronous.csv")


data = purrr::map_df(files, read_hr)

write.csv(data, file = "Real data.csv")




### We want to downsample the data 


small_data= data_hr %>%
    group(n = 100, method = 'greedy') %>%
    dplyr::summarise(
      study = study[[1]],
      group = group[[1]],
      trial = trial[[1]],
      condition = condition[[1]],
       time = mean(time,na.rm=T),
       hr1 = mean(hr1,na.rm=T),
       hr2 = mean(hr2,na.rm=T),
       resp1 = mean(resp1,na.rm=T),
       resp2 = mean(resp2,na.rm=T))


write.csv(small_data, file = "downsized_data.csv")



```

#### PREPROCESSING 

- Load the logs
- Produce a plot of the participants' respiration signal and a different one of the participants' HR signal (for inspecting whether the data is usable)
  N.B: remember the slides: artifacts, downsampling, scaling.
  N.B. The gridExtra::grid.arrange() function allows you to display the plots side by side. E.g. grid.arrange(plot1, plot2, plot3, ncol=3)
- Can you eye-ball which condition if any displays more physiological coordination?

```{r}
### loading the downsampled data 

##### removing outliers (function) HEARTRATE    

data_new<-read.csv("downsized_new_data.csv", sep=",", header = T) %>%
  filter(complete.cases(hr1),
         complete.cases(resp1),
         complete.cases(resp2),
         complete.cases(hr2))


########################################################
#OUTLIERS - removing outliers 

remove <- function(ts,threshold){
  ts[ts > (mean(ts,na.rm=T) + 
             (threshold*sd(ts,na.rm=T))) |
       ts < (mean(ts,na.rm=T) -
               (threshold*sd(ts,na.rm=T)))] = mean(ts,na.rm=T)
  return(ts)
  }


threshold=1  

##HR 
data_new$hr1=remove(data_new$hr1,threshold)
data_new$hr2=remove(data_new$hr2,threshold)

##Resp
data_new$resp1=remove(data_new$resp1,threshold)
data_new$resp2=remove(data_new$resp2,threshold)

#SCALE
my_scale = function(x) (x - mean(x)) / sd(x)

data_scale_new <- data_new %>%
  group_by(condition, group) %>%
  mutate_at(vars("hr1", "hr2", "resp1", "resp2"), my_scale)

write.csv(data_scale_new, file = "data_scale_new.csv")

########################################################################
#Run from here, load in the preprossed data aka removed outliers, scaled and downsampled
data_scale_new <- read.csv("data_scale_new.csv")


###Plot of all hr and resp data
plot_hr_new = ggplot(data_scale_new, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") + 
  labs(title = "all HR data all conditions")
plot_hr_new

plot_resp_new = ggplot(data_scale_new, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") +
  labs(title = "all RESP data all conditions")
plot_resp_new


##### removing outliers (function) RESPIRATION - Skal dette gemmes? For vi har jo en overordnet function ovenover, hvor vi fjerner outliers for både hr og resp??ssss

remove_resp <- function(ts,threshold){
  ts[ts > (mean(ts,na.rm=T) + 
             (threshold*sd(ts,na.rm=T))) |
       ts < (mean(ts,na.rm=T) -
               (threshold*sd(ts,na.rm=T)))] = mean(ts,na.rm=T)
  return(ts)
  }


threshold_resp=2.2 

#remove respiration outliers 

plot_resp_new = ggplot(data_scale_new, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red")
plot_resp_new

#Removing columns
data_scale_new$X = NULL
data_scale_new$.groups = NULL
data_scale_new$X.1 = NULL


#We select two groups
g6<-data_scale_new %>%         
  filter(group=="G6")

g4<-data_scale_new %>%         
  filter(group=="G4")

#Group 6
#HR
plot_hr_g6 = ggplot(g6, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") +
  labs(title = "Figure 1: HR for group 6")
plot_hr_g6

#Resp
plot_resp_g6 = ggplot(g6, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") + 
  labs(title = "Figure 2: RESP for group 6")
plot_resp_g6

#Group 4
#HR
plot_hr_g4 = ggplot(g4, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") +
  labs(title = "Figure 1: HR for group 4")
plot_hr_g4

#Resp
plot_resp_g4 = ggplot(g4, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") + 
  labs(title = "Figure 2: RESP for group 4")
plot_resp_g4

```


```{r}

#We create 3 dataframes based on the 3 conditions for group 6

#Sync
sync<-g6 %>%         
  filter(condition=="Synchronous.")

#HR
plot_hr_g6_sync = ggplot(sync, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") +
  labs(title = "Figure 3: HR g6 sync")
plot_hr_g6_sync

#Resp
plot_resp_g6_sync = ggplot(sync, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") + 
  labs(title = "Figure 4: RESP g6 sync")
plot_resp_g6_sync

#Conversation
conv<-g6 %>%         
  filter(condition=="Conversation.")

#HR
plot_hr_g6_conv = ggplot(conv, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") + 
  labs(title = "Figure 5: HR g6 conv")
plot_hr_g6_conv

#Resp
plot_resp_g6_conv = ggplot(conv, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") +
  labs(title = "Figure 6: RESP g6 conv")
plot_resp_g6_conv

#Turntaking
turn<-g6 %>%         
  filter(condition=="TurnTaking.")

#HR
plot_hr_g6_turn = ggplot(turn, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") + 
  labs(title = "Figure 7: HR g6 turnT")
plot_hr_g6_turn

#Resp
plot_resp_g6_turn = ggplot(turn, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") +
  labs(title = "Figure 8: RESP g6 turnT")
plot_resp_g6_turn

```

```{r}

#We create 3 dataframes based on the 3 conditions for group 4

#Sync
sync_g4<-g4 %>%         
  filter(condition=="Synchronous.")

#HR
plot_hr_g4_sync = ggplot(sync_g4, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") +
  labs(title = "Figure 3: HR g4 sync")
plot_hr_g4_sync

#Resp
plot_resp_g4_sync = ggplot(sync_g4, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") + 
  labs(title = "Figure 4: RESP g4 sync")
plot_resp_g4_sync

#Conversation
conv_g4<-g4 %>%         
  filter(condition=="Conversation.")

#HR
plot_hr_g4_conv = ggplot(conv_g4, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") + 
  labs(title = "Figure 5: HR g4 conv")
plot_hr_g4_conv

#Resp
plot_resp_g4_conv = ggplot(conv_g4, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") +
  labs(title = "Figure 4: RESP g4 conv")
plot_resp_g4_conv

#Turntaking
turn_g4<-g4 %>%         
  filter(condition=="TurnTaking.")

#HR
plot_hr_g4_turn = ggplot(turn_g4, aes(time)) +
  geom_line(aes(y = hr1), colour = "blue") +
  geom_line(aes(y = hr2), colour = "red") + 
  labs(title = "Figure 7: HR g4 turnT")
plot_hr_g4_turn

#Resp
plot_resp_g4_turn = ggplot(turn_g4, aes(time)) +
  geom_line(aes(y = resp1), colour = "blue") +
  geom_line(aes(y = resp2), colour = "red") +
  labs(title = "Figure 8: RESP g4 turnT")
plot_resp_g4_turn

```

### Exploring physiological signals
```{r}

#We get an overview of the plots 

#g6
plot_g6 <- gridExtra::grid.arrange(plot_resp_g6_sync, plot_resp_g6_conv, plot_resp_g6_turn, plot_hr_g6_sync, plot_hr_g6_conv, plot_hr_g6_turn, nrow = 2, ncol = 3)

#g4
plot_g4 <- gridExtra::grid.arrange(plot_resp_g4_sync, plot_resp_g4_conv, plot_resp_g4_turn, plot_hr_g4_sync, plot_hr_g4_conv, plot_hr_g4_turn, nrow = 2, ncol = 3)

```


```{r}
#### Run the crqa on hr and resp  
###GROUP 6

par = list(lgM = 50, steps = seq(1, 6, 1), radiusspan = 100, radiussample = 40, normalize = 0, rescale = 0, mindiagline = 2, minvertline = 2, tw = 0, whiteline = FALSE, recpt = FALSE, fnnpercent = 10, typeami = "mindip")

#optimizing parameters for hr
ans_hr = try(optimizeParam(g6$hr1, g6$hr2, par, min.rec = 2, max.rec = 5 )) 

ans_hr
#answer = NULL

#optimizing parameters for resp
ans_resp = try(optimizeParam(g6$resp1, g6$resp2, par, min.rec = 2, max.rec = 5))

ans_resp

#R=0.3581349, em=2, delay= 16

#crqa hr
results_hr = crqa (g6$hr1, g6$hr2, delay=5, embed=20, radius=2.723886,normalize=0,rescale=0,mindiagline = 2,minvertline = 2)

results_hr$RR


#crqa resp
results_resp = crqa (g6$resp1, g6$resp2, delay=16, embed=2, radius= 0.3581349, normalize=0,rescale=0,mindiagline = 2,minvertline = 2)

results_resp$RR
# 3.826752


###RP PLOT RESP G6
RP_resp_g6 <- results_resp$RP
cols <- c("white", "blue4")
image(RP_resp_g6) 

###RP PLOT HR G6
RP_hr_g6 <- results_hr$RP
cols <- c("white", "blue4")
image(RP_hr_g6)

### G4, in order to make the RP

par_g4 = list(lgM = 50, steps = seq(1, 6, 1), radiusspan = 400, radiussample = 120, normalize = 0, rescale = 0, mindiagline = 2, minvertline = 2, tw = 0, whiteline = FALSE, recpt = FALSE, fnnpercent = 10, typeami = "mindip")

#optimizing parameters for hr
ans_hr_g4 = try(optimizeParam(g4$hr1, g4$hr2, par_g4, min.rec = 2, max.rec = 5 )) 

ans_hr_g4

#optimizing parameters for resp
ans_resp_g4 = try(optimizeParam(g4$resp1, g4$resp2, par_g4, min.rec = 1, max.rec = 10))

ans_resp_g4


#crqa hr
results_hr_g4 = crqa (g4$hr1, g4$hr2, delay=46, embed=11, radius=2.294891
,normalize=0,rescale=0,mindiagline = 2,minvertline = 2)

#crqa resp
results_resp_g4 = crqa (g4$resp1, g4$resp2, delay=16, embed=2, radius= 0.3581349, normalize=0,rescale=0,mindiagline = 2,minvertline = 2) #Not changed, because it will not work


###RP PLOT RESP G4
RP_resp_g4 <- results_resp_g4$RP
cols <- c("white", "blue4")
image(RP_resp_g4) #Haven't run, because it will not run the parameters

###RP PLOT HR G4
RP_hr_g4 <- results_hr_g4$RP
cols <- c("white", "blue4")
image(RP_hr_g4) 

####### Crqa on the whole dataset

#function to read Resp
cr_func_resp <- function(resp1, resp2){
  ans_resp2 = list(NA, NA, NA)
  ans_resp2 = try(optimizeParam(resp1, resp2, par, min.rec = 2, max.rec = 5))
  delay=as.numeric(ans_resp2[3])
  embed=as.numeric(ans_resp2[2])
  radius=as.numeric(ans_resp2[1])
  return(as.data.frame(ans_resp2))
  }

params_resp = data_scale_new %>%
  group_by(group, condition) %>%
  summarise(params = list(cr_func_resp(resp1, resp2))) %>%
  unnest()
#Optimal Radius Not found: try again choosing a wider radius span and larger sample size

#we need all the data from crqa in a df and then take the median(more robust to outliers, not want outliers to influence overall results) from each group across conditions 

#median 
median(params_resp$radius)
median(params_resp$emddim)
median(params_resp$delay)
#Error pga. ovenstående 


#function to read hr 
cr_func_hr <- function(hr1, hr2){
  ans_hr2 = list(NA, NA, NA)
  ans_hr2 = try(optimizeParam(hr1, hr2, par, min.rec = 2, max.rec = 5))
  delay=as.numeric(ans_hr2[3])
  embed=as.numeric(ans_hr2[2])
  radius=as.numeric(ans_hr2[1])
  return(as.data.frame(ans_hr2))
  }

params_hr = data_scale_new %>%
  group_by(group, condition) %>%
  summarise(params = list(cr_func_hr(hr1, hr2))) %>%
  unnest()
##Optimal Radius Not found: try again choosing a wider radius span and larger sample size


#median 
median(params_hr$radius)
median(params_hr$emddim)
median(params_hr$delay)


#HR
#crqa hr on the whole dataset
run_crqa_hr = function(hr1, hr2) {
  result_hr = crqa(hr1, hr2, delay=19, embed=11, radius=2.633407, normalize=0,rescale=0,mindiagline = 2,minvertline = 2)
    return(data.frame(RR = result_hr$RR, DET = result_hr$DET, NRLINE = result_hr$NRLINE, maxL = result_hr$maxL, L = result_hr$L, ENTR = result_hr$ENTR, rENTR = result_hr$rENTR, LAM = result_hr$LAM, TT = result_hr$TT)) 
}

crqa_hr_all <- data_scale_new %>% 
  group_by(condition, group) %>%
  summarise(result_hr = list(run_crqa_hr(hr1, hr2))) %>%
  mutate(baseline = "real") %>%
  unnest()


  #RESP
  #crqa resp function on the whole dataset
  run_crqa_resp = function(resp1, resp2) {
    result_resp = crqa(resp1, resp2, delay=27, embed=2, radius=0.448711, normalize=0,rescale=0,mindiagline = 2,minvertline = 2)
    return(data.frame(RR = result_resp$RR, DET = result_resp$DET, NRLINE = result_resp$NRLINE, maxL = result_resp$maxL, L = result_resp$L, ENTR = result_resp$ENTR, rENTR = result_resp$rENTR, LAM = result_resp$LAM, TT = result_resp$TT)) 
  }
  
  
  crqa_resp_all <- data_scale_new %>%
    group_by(condition, group) %>%
    summarise(result_resp = list(run_crqa_resp(resp1, resp2))) %>%
    mutate(baseline = "real") %>%
    unnest()



#SHUFFLE PAIRS
  #crqa hr on the whole dataset
run_crqa_hr_shuffle = function(hr1, hr2) {
  result_hr_shuffle = crqa(sample(hr1), sample(hr2), delay=19, embed=11, radius=2.633407, normalize=0,rescale=0,mindiagline = 2,minvertline = 2)
  return(data.frame(RR = result_hr_shuffle$RR, DET = result_hr_shuffle$DET, NRLINE = result_hr_shuffle$NRLINE, maxL = result_hr_shuffle$maxL, L = result_hr_shuffle$L, ENTR = result_hr_shuffle$ENTR, rENTR = result_hr_shuffle$rENTR, LAM = result_hr_shuffle$LAM, TT = result_hr_shuffle$TT, RP = result_hr_shuffle$RP)) 
}

crqa_hr_all_shuffle <- data_scale_new %>%
  group_by(condition, group) %>%
  summarise(result_hr_shuffle = list(run_crqa_hr_shuffle(hr1, hr2))) %>%
  mutate(baseline = "shuffle") %>%
  unnest()
#Error in summarise_impl(.data, dots) : 
  #Evaluation error: cannot coerce class <U+0091>structure("ngCMatrix", package = "Matrix")<U+0092> to a data.frame.



#crqa resp function on the whole dataset
run_crqa_resp_shuffle = function(resp1, resp2) {
  result_resp_shuffle = crqa(sample(resp1), sample(resp2), delay=27, embed=2, radius=0.448711, normalize=0,rescale=0,mindiagline = 2,minvertline = 2)
  return(data.frame(RR = result_resp_shuffle$RR, DET = result_resp_shuffle$DET, NRLINE = result_resp_shuffle$NRLINE, maxL = result_resp_shuffle$maxL, L = result_resp_shuffle$L, ENTR = result_resp_shuffle$ENTR, rENTR = result_resp_shuffle$rENTR, LAM = result_resp_shuffle$LAM, TT = result_resp_shuffle$TT, RP = result_resp_shuffle$RP)) 
}

crqa_resp_all_shuffle <- data_scale_new %>%
  group_by(condition, group) %>%
  summarise(result_resp_shuffle = list(run_crqa_resp_shuffle(resp1, resp2))) %>%
  mutate(baseline = "shuffle") %>%
  unnest()
#Error in summarise_impl(.data, dots) : 
  #Evaluation error: cannot coerce class <U+0091>structure("ngCMatrix", package = "Matrix")<U+0092> to a data.frame.


resp <- rbind(crqa_resp_all, crqa_resp_all_shuffle)
write.csv(resp, file = "resp_real_shuffle") #the csv files are just so we do not have to run all the functions when starting R 
resp_csv <- read.csv("resp_real_shuffle")

hr <- rbind(crqa_hr_all, crqa_hr_all_shuffle)
write.csv(hr, file = "hr_real_shuffle")
hr_csv <- read.csv("hr_real_shuffle")



```


```{r}
#SURROGATE PAIRS BLACK FRIDAY

data_long_hr = data_scale_new %>%
  select(-resp1, -resp2, -ecg1, -ecg2) %>%
  gather("p", "hr", hr1, hr2) %>%
  mutate(p = ifelse(p == "hr1", 1, 2)) 

data_long_resp = data_scale_new %>%
  select(-hr1, -hr2, -ecg1, -ecg2) %>%
  gather("p", "resp", resp1, resp2) %>%
  mutate(p = ifelse(p == "resp1", 1, 2))

data_long = full_join(data_long_hr, data_long_resp) %>%
  mutate(participant = as.numeric(as.factor(str_c(group, p))))

participants = unique(data_long$participant)

combinations = expand.grid(p1=participants, p2=participants, cond = unique(data_long$condition)) %>%
  filter(p1<p2) %>%
  mutate(cond = as.character(cond))


## HEART RATE 
crqa_surrogate_hr = function(p1, p2, cond) {
  print(str_c(p1, " vs ", p2, " condition ", cond))
  d1 = filter(data_long, participant == p1, condition == cond)
  d2 = filter(data_long, participant == p2, condition == cond)
  run_crqa_hr(d1$hr, d2$hr)
}


#the following takes around 30 minutes
surrogate_results_hr = cbind(combinations, 
                          pmap_df(list(combinations$p1, combinations$p2, combinations$cond), crqa_surrogate_hr)) %>%

  rename(condition = cond) %>%
  select(-p1, -p2)


write.csv(surrogate_results_hr, file = "surrogate_results_hr.csv") #made today 28/11

hr_surro <- read.csv("surrogate_results_hr.csv")
hr_surro$X = NULL

hr_surro <- mutate(hr_surro, baseline = "surrogate") #if this does not work, don't worry
hr_surro <- subset(hr_surro, select =c(condition, baseline, RR:TT)) #same^



#RESPIRATION 
crqa_surrogate_resp = function(p1, p2, cond) {
  print(str_c(p1, " vs ", p2, " condition ", cond))
  d1 = filter(data_long, participant == p1, condition == cond)
  d2 = filter(data_long, participant == p2, condition == cond)
  run_crqa_resp(d1$resp, d2$resp)
}

#this one also takes a long time
surrogate_results_resp = cbind(combinations, 
                          pmap_df(list(combinations$p1, combinations$p2, combinations$cond), crqa_surrogate_resp)) %>%

  rename(condition = cond) %>%
  select(-p1, -p2)


write.csv(surrogate_results_resp, file = "surrogate_results_resp.csv") #the first to run this script after 23.11 should overwrite this csv!

resp_surro <- read.csv("surrogate_results_resp.csv")
resp_surro$X = NULL

resp_surro <- mutate(resp_surro, baseline = "surrogate")
resp_surro <- subset(resp_surro, select =c(condition, baseline, RR:TT))



#Rbind all baselines together 

#We delete the group column in both hr and resp 

hr$group = NULL

resp$group = NULL

hr <- full_join(hr, hr_surro)

write.csv(hr, file = "hr_all.csv") 

hr <- read.csv("hr_all.csv")

#resp

resp <- full_join(resp, resp_surro)

write.csv(resp, file = "resp_all.csv") 

resp <- read.csv("resp_all.csv")


```


### Testing effects of conditions
 - make a (probably underpowered) mixed model testing effects of the different conditions on heart rate and respiration coordination
 - N.B: would it make sense to include surrogate pairs? and if so how? what would that tell you?
 
 
```{r}
#####HEART RATE

#MODEL FOR RR
hr_RR <- lm(RR ~ condition*baseline, data = hr)
summary(hr_RR)


#MODEL FOR L
hr_L <- lm(L ~ condition*baseline, data = hr)
summary(hr_L)


#MODEL FOR DET 
hr_DET <- lm(DET ~ condition*baseline, data = hr)
summary(hr_DET)

#mean 86.48751
hr$RR


####RESPIRATION 

#MODEL FOR RR
re_RR <- lm(RR ~ condition*baseline, data = resp)
summary(re_RR)


#MODEL FOR L
re_L <- lm(L ~ condition*baseline, data = resp)
summary(re_L)


#MODEL FOR DET 
re_DET <- lm(DET ~ condition*baseline, data = resp)
summary(re_DET)


#What we need to analyse

#real + surro 
#opgave corralted eller interactiv correlated?? 

#real + shuffle 
#shuffle er random level og real pairs er "in order", foelger den rigtige times series 

#sh + surro does not make sense to analyse 

```

### Effects of respiration coordination on heart rate coordination
 - describe how you would test those.
 - Optional: run the models and report them
 
```{r}
##PREDICTIONS HEART RATE
predictions_hr <- distinct(hr, condition, baseline)

##RR
predictions_hr_RR = cbind(predictions_hr, predict(hr_RR, newdata = predictions_hr, interval = "confidence"))


##L 
predictions_hr_L = cbind(predictions_hr, predict(hr_L, newdata = predictions_hr, interval = "confidence"))


##DET 
predictions_hr_DET =cbind(predictions_hr, predict(hr_DET, newdata = predictions_hr, interval = "confidence"))



##PREDICTIONS FOR RESPIRATION
predictions_resp = distinct(resp, condition, baseline)


##RR
predictions_resp_RR = cbind(predictions_resp, predict(re_RR, newdata = predictions_resp, interval = "confidence"))

##L 
predictions_resp_L = cbind(predictions_resp, predict(re_L, newdata = predictions_resp, interval = "confidence"))

##DET
predictions_resp_DET = cbind(predictions_resp, predict(re_DET, newdata = predictions_resp, interval = "confidence"))

#PLOT FOR HR DATA ALL PAIRS AND ALL CONDITIONS 

#RR
plot_HR_RR = ggplot(predictions_hr_RR, aes(x = baseline, fill = baseline, y = fit)) +
  geom_bar(stat = "identity") + 
  geom_errorbar(aes(ymin = lwr, ymax = upr), width = .3) +
  facet_wrap(~ condition) +
  labs(title = "HR RR plot")
plot_HR_RR

#L
plot_HR_L = ggplot(predictions_hr_L, aes(x = baseline, fill = baseline, y = fit)) +
  geom_bar(stat = "identity") + 
  geom_errorbar(aes(ymin = lwr, ymax = upr), width = .3) +
  facet_wrap(~ condition) +
  labs(title = "HR L plot")
plot_HR_L

#DET
plot_HR_DET = ggplot(predictions_hr_DET, aes(x = baseline, fill = baseline, y = fit)) +
  geom_bar(stat = "identity") + 
  geom_errorbar(aes(ymin = lwr, ymax = upr), width = .3) +
  facet_wrap(~ condition) +
  labs(title = "HR DET plot")
plot_HR_DET

#RESP

#RR
plot_resp_RR = ggplot(predictions_resp_RR, aes(x = baseline, fill = baseline, y = fit)) +
  geom_bar(stat = "identity") + 
  geom_errorbar(aes(ymin = lwr, ymax = upr), width = .3) +
  facet_wrap(~ condition) +
  labs(title = "Resp RR plot")
plot_resp_RR

#L
plot_resp_L = ggplot(predictions_resp_L, aes(x = baseline, fill = baseline, y = fit)) +
  geom_bar(stat = "identity") + 
  geom_errorbar(aes(ymin = lwr, ymax = upr), width = .3) +
  facet_wrap(~ condition) +
  labs(title = "Resp L plot")
plot_resp_L

#DET
plot_resp_DET = ggplot(predictions_resp_DET, aes(x = baseline, fill = baseline, y = fit)) +
  geom_bar(stat = "identity") + 
  geom_errorbar(aes(ymin = lwr, ymax = upr), width = .3) +
  facet_wrap(~ condition) +
  labs(title = "Resp DET plot")
plot_resp_DET

#HR
plot_HR <- gridExtra::grid.arrange(plot_HR_L, plot_HR_RR, plot_HR_DET, nrow = 3, ncol = 1)

plot_resp <- gridExtra::grid.arrange(plot_resp_L, plot_resp_RR, plot_resp_DET, nrow = 3, ncol = 1)


#Plotting timeeeeeeee

#we need a plot like the one set in the group, try to read the notes to make a plot. it should be a barplot with facetwrap


####N og K's noter, må ikke slettes!!
#PLOT 

#predictions$RR = predict(mixed, newdata = predictions, interval = "confidence")con på x akse 
#rr på y akse 
#bar plot 
#facet wrapt eller forskellige farver 

#model predictions som title 

#9 eows 
#predict(model, newdata= )

#newdata = der skal være 9 rækker 
#det giver 9 prikker i plot med usikkerheder (error bars)


#malte noter
#predictions = distinct(hr, condition, baseline)

#predictions$RR = predict(mixed, newdata = predictions, interval = "confidence")
#RR, L og DET 

#ggplot(aes(x=condition), data =predictions)+ 
 # geom_bar(color = "baseline")


```
 
 