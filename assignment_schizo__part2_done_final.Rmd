---
title: "Assignment 3 - Part 2 - Diagnosing Schizophrenia from Voice"
author: "Riccardo Fusaroli"
date: "October 17, 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Assignment 3 - Diagnosing schizophrenia from voice

In the previous part of the assignment you generated a bunch of "features", that is, of quantitative descriptors of voice in schizophrenia, focusing on pitch.
In the course of this assignment we will use them to try to automatically diagnose schizophrenia from voice only, that is, relying on the set of features you produced last time, we will try to produce an automated classifier.


```{r}
#load all the shit 

#install.packages("tidyverse")
library(tidyverse)

#install.packages("ggplot2")
library(ggplot2)

#install.packages("data.table")
library(data.table)

#install.packages("caret")
library(caret)

#install.packages("stringr")
library(stringr)

#install.packages("lme4")
library(lme4)

#install.packages("robustHD")
library(robustHD)

#install.packages("psycho")
library(psycho)

#install.packages("boot")
library(boot)

#install.packages("pROC")
library(pROC)

#install.packages("knitr")
library(knitr)

#devtools::install_github("LudvigOlsen/groupdata2")
library(groupdata2)

#devtools::install_github("LudvigOlsen/cvms")
library(cvms)

#https://github.com/LudvigOlsen/cvms

#install.packages("dplyr")
library(dplyr)


data<-read.csv("datavol2.csv", header=T, sep=",")

data <- mutate(data, range = max - min)

#relable or rescale the data in range to make the model work 
#we need to change the scale without changing the data!
#min-max scale 
#z-scaling, how to: mean-sd
#OBS log odds are weird!!! translate to probabilixc scale and keep in mind that the data are 
#change of overall scale but not relation between the data point 

#make new dataset with rescaling 


data_scale<-read.csv("datavol2.csv", header=T, sep=",")


data_scale <- mutate(data, range = max - min)

#re-scaling the range column as the range is too big 

data_scale <- data_scale %>% 
  psycho::standardize() 

data_scale$X = NULL


```


### Question 1: Can you diagnose schizophrenia from pitch range only? If so, how well?

1. Build a logistic regression to see whether you can diagnose schizophrenia from pitch range only.
```{r}

#creating a logistic regression model 
logreg_model = glmer(diagnosis ~ range + (1|subject), data = data_scale, family = "binomial") #family due to diagnosis, which only has two options 
summary(logreg_model)



#can we diagnose from pitch range? yes? no, why not? 

#REPORT RESULTS, COPY
#Based on our results from the logistic regression model we can significantly diagnose schizophrenia from pitch range only, including a random intercept for subject 
#As the p-value of the output shows the analysis is very significant with a p-value of 0.00642 


```


2. Calculate the different performance measures (accuracy, sensitivity, specificity, PPV, NPV, ROC curve) on a logistic regression using the full dataset. Don't forget the random effects!
```{r}

#calculate the performance of the model above 

predict1 <- inv.logit(predict(logreg_model))

data_scale <- mutate(data_scale, predictions = predict1)


#slide 51, decision rule
data_scale$predictions_perc=inv.logit(predict(logreg_model))

data_scale$predictions[data_scale$predictions_perc>0.5]="D0"

data_scale$predictions[data_scale$predictions_perc<=0.5]="D1" 

data_scale$predictions <- as.factor(data_scale$predictions)

# data_scale$diagnosis <- as.factor(data_scale$diagnosis)

confusionMatrix(data = data_scale$predictions, reference = data_scale$diagnosis, positive = "D1")


data_scale$predictions


#confusionMatrix(data = Data1$Predictions, reference = Data1$diagnosis, positive = "ASD")

#something about curves and area under the curve, changing the numbers above and then what happens 

#C S S C S 
#data_scale$diagnosis


###############


#predict1 <- predict(logreg_model, type ="response", data_scale) 

#data_scale <- mutate(data_scale, predictions = predict1)

#data_scale$predictions <- as.factor(data_scale$predictions)

#class(data_scale$predictions)


accuracy = (339+178)/(339+178+497+325) = 0.386109
sensitivity(data = data_scale$predictions, reference = data_scale$diagnosis, positive = "D1")
specificity(data = data_scale$predictions, reference = data_scale$diagnosis, negative = "D0")
posPredValue(data = data_scale$predictions, reference = data_scale$diagnosis, positive = "D1") #PPV
negPredValue(data = data_scale$predictions, reference = data_scale$diagnosis, negative = "D0") #NPV
#ROC curve 


curve <- roc(response = data_scale$diagnosis, predictor = data_scale$predictions_perc)

auc(curve)
ci(curve)
plot(curve, legacy.axes = TRUE)



```
REPORT THE PERFORMANCE MEASURES

Confusion Matrix and predictions: 
          Reference
Prediction  D0  D1
        D0 339 497
        D1 325 178
        
PLOT


3. Then cross-validate the logistic regression and re-calculate performance on the testing folds. N.B. The cross-validation functions you already have should be tweaked: you need to calculate these new performance measures.

N.B. the predict() function generates log odds (the full scale between minus and plus infinity). Log odds > 0 indicates a choice of 1, below a choice of 0.
N.B. you need to decide whether calculate performance on each single test fold or save all the prediction for test folds in one dataset, so to calculate overall performance.
N.B. Now you have two levels of structure: subject and study. Should this impact your cross-validation?

```{r}

supersigne <- fold(data_scale, k = 70,
             cat_col = "diagnosis",
             id_col = "subject") %>% 
  arrange(.folds)


#The issue was with "diagnosis" being a factor. To solve this we create a new column with 1's and 0's 
supersigne$D <- 1
supersigne$D[supersigne$diagnosis=="D0"] = 0

cross_val <- cross_validate(supersigne, "D ~ range + (1|subject)", 
                     folds_col = '.folds', 
                     family="binomial")

cross_val


```


### Question 2 - Which single acoustic predictor is the best predictor of diagnosis?

Based on a glmer() model analysis on each single acoustic feature assess which is the best predictor of diagnosis. All acoustic features beside maximum pitch are significant, however the minimum pitch seems to be the best predictor of diagnosis with a p-value of 6.49e-15. See table 1 for all the resutls. 

CREATE TABLE 1 

```{r}
#range 
range_p<- glmer(diagnosis ~ range + (1|subject),data = data_scale, family = "binomial") 
summary(range_p) 
#range       -0.17130    0.06285  -2.726  0.00642 **


#Standard deviation 
sd_p<- glmer(diagnosis ~ sd + (1|subject),data = data_scale, family = "binomial") 
summary(sd_p) 
#sd          -0.12413    0.06308  -1.968   0.0491 *


#minimun pitch 
min_p<- glmer(diagnosis ~ min + (1|subject),data = data_scale, family = "binomial") 
summary(min_p) 
#min          0.82997    0.10649   7.794 6.49e-15 ***


#maximum pitch
max_p<- glmer(diagnosis ~ max + (1|subject),data = data_scale, family = "binomial") 
summary(max_p) 
#max          0.02740    0.06515   0.421    0.674


#median of pitch
median_p<- glmer(diagnosis ~ median + (1|subject),data = data_scale, family = "binomial") 
summary(median_p) 
#median       0.49330    0.14498   3.403 0.000668 ***


#iqr 
iqr_p<- glmer(diagnosis ~ iqr + (1|subject),data = data_scale, family = "binomial") 
summary(iqr_p) 
#iqr         -0.16171    0.07030  -2.300   0.0214 *

```

### Question 3 - Which combination of acoustic predictors is best for diagnosing schizophrenia?

Now it's time to go wild! Use all (voice-related) variables and interactions you can think of. Compare models and select the best performing model you can find.

Remember:
- Out-of-sample error crucial to build the best model!
- After choosing the model, send Malte and Riccardo the code of your model

```{r}

#testing the best (significant) acustic predictors 
wild_model <- glmer(diagnosis ~ min * median + range (1|subject),data = data_scale, family = "binomial") 
summary(wild_model)

wild_model <- glmer(diagnosis ~ min + median * range + (1|subject),data = data_scale, family = "binomial") 
summary(wild_model)

wild_model <- glmer(diagnosis ~ min + median + range (1|subject),data = data_scale, family = "binomial") 
summary(wild_model)

wild_model <- glmer(diagnosis ~ min * range + median + (1|subject),data = data_scale, family = "binomial") 
summary(wild_model)



#BEST
wild_model <- glmer(diagnosis ~ min * median + (1|subject),data = data_scale, family = "binomial") 
summary(wild_model)

```
Range is not very good as a related predictor since maximum is a part of the range. Range as a single predictor is significant, however an interaction between range and the median or the interaction between the minimum and range is not significant, therefore we evalute that range is not going to be a part of our wild model. 

### Question 4: Properly report the results

METHODS SECTION: how did you analyse the data? That is, how did you extract the data, designed the models and compared their performance?

RESULTS SECTION: can you diagnose schizophrenia based on voice? which features are used? Comment on the difference between the different performance measures.

